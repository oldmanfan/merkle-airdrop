
const MerkleTools = require('merkle-tools');

var keccak256 = require('js-sha3').keccak256;

const treeOptions = {
    hashType: 'keccak256' // optional, defaults to 'sha256'
}

class MerkleTree {
    constructor() {
        this.merkleTools = new MerkleTools(treeOptions);
        this.leaves = [];
    }

    addLeaves(leaves) {
        for (let leaf of leaves) {
            this.addLeaf(leaf);
        }
    }

    addLeaf(leaf) {
        const lowerAddress = leaf.address.toLowerCase();
        const address  = lowerAddress.startsWith('0x') ? lowerAddress.substring(2) : lowerAddress;

        const lowerAmount = leaf.amount.toLowerCase();
        const amountStr = lowerAmount.startsWith('0x') ? lowerAmount.substring(2) : lowerAmount;
        const paddingAmount = '0'.repeat(64 - amountStr.length) + amountStr;

        const v = Buffer.from(address + paddingAmount, 'hex');
        const hash = Buffer.from(keccak256.array(v));
        this.merkleTools.addLeaf(hash);

        this.leaves.push(leaf);
    }

    makeTree() {
        this.merkleTools.makeTree();
    }

    getIndexOf(leaf) {
        if (typeof leaf === 'string') {
            for (let i = 0; i < this.leaves.length; i++) {
                if (this.leaves[i].address === leaf) {
                   return i;
                }
            }
        }

        throw new Error('Not Found or Type is not support');
    }

    getMerkleRoot() {
        return '0x' + this.merkleTools.getMerkleRoot().toString('hex');
    }

    getProofsOf(leaf) {
        let index = leaf;
        if (typeof leaf === 'string') {
           index = this.getIndexOf(leaf);
        }

        var nodes = [];
        const proofs = this.merkleTools.getProof(index);
        proofs.forEach(p => {
            nodes.push([
                p.left ? '0x' + p.left.toString('hex') : '0x' + p.right.toString('hex'),
                !!p.left
            ]);
        });
        return nodes;
    }

    getHashOf(leaf) {
        let index = leaf;
        if (typeof leaf === 'string') {
            index = this.getIndexOf(leaf);
        }
        return '0x' + this.merkleTools.getLeaf(index).toString('hex');
    }

    test(idx) {
        const node = this.merkleTools.getLeaf(idx).toString('hex');
        const root = this.merkleTools.getMerkleRoot().toString('hex');
        var nodes = [];
        const proofs = this.merkleTools.getProof(idx);
        proofs.forEach(p => {
            const key = Object.keys(p)[0];
            const v = {key: p[key].toString('hex') }
            nodes.push(v);
        });
        return this.merkleTools.validateProof(proofs, node, root);
    }
}

module.exports = {MerkleTree}
