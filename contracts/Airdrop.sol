//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract Airdrop {

    bytes32 public merkleRoot;

    function setMerkleRoot(bytes32 root) public {
        merkleRoot = root;
    }

    struct MerkleNode {
        bytes32 proof;
        bool    left;
    }
    function getAirdrop(MerkleNode[] memory proofs, uint256 amount) public view returns(bool) {
        bytes32 pRoot = keccak256(abi.encodePacked(msg.sender, amount));
        for (uint i = 0; i < proofs.length; i++) {
            MerkleNode memory node = proofs[i];
            if (node.left) {
                pRoot = keccak256(abi.encodePacked(node.proof, pRoot));
            } else {
                pRoot = keccak256(abi.encodePacked(pRoot, node.proof));
            }
        }

        return pRoot == merkleRoot;
    }
}