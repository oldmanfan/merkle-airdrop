// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { BigNumber } = require("ethers");
const {ethers } = require("hardhat");
const { MerkleTree } = require("../src/merkle");

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const players = await ethers.getSigners();

  const Airdrop = await ethers.getContractFactory("Airdrop");
  const airdrop = await Airdrop.deploy();

  await airdrop.deployed();

  const m = new MerkleTree();
  // 添加airdrop对象信息: { address: '0xabcdefgh', amount: '0x123456' }
  for (let player of players) {
    m.addLeaf({address: player.address, amount: BigNumber.from('1000000000000000').toHexString()});
  }
  m.makeTree();

  await airdrop.setMerkleRoot(m.getMerkleRoot());

  // 开始测试吧
  const player1 = players[5];
  const player2 = players[6];

  const proofs = m.getProofsOf(player1.address);
  let r = await airdrop.connect(player1).getAirdrop(proofs, BigNumber.from('1000000000000000'));
  console.log(`账号和数量都正确, 返回true: ${r}`);

  r = await airdrop.connect(player1).getAirdrop(proofs, BigNumber.from('1000000000000002'));
  console.log(`账号正确,数量错误, 返回false: ${r}`);

  r = await airdrop.connect(player2).getAirdrop(proofs, BigNumber.from('1000000000000000'));
  console.log(`账号错误,数量正确, 返回false: ${r}`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
